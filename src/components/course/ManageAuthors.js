import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as authorActions from '../../actions/authorActions';
import CourseForm from './CourseForm';

import SelectInput from '../common/SelectInput';

import {authorsFormatterForDropdown} from '../../selectors/selectors';
import toastr from 'toastr';

export class ManageAuthorPage extends React.Component {
  constructor(props, contex) {
    super(props, contex);

    this.state = {
      authors: Object.assign([], this.props.authors),
      errors: {},
      saving: false,
      author: ''
    };
    this.updateAuthorState = this.updateAuthorState.bind(this);
    this.deleteAuthor = this.deleteAuthor.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.authors != nextProps.authors) {
      this.setState({authors: Object.assign([], nextProps.authors)});
    }
  }

  updateAuthortate(event) {
    let author = event.target.value;

    return this.setState({author: author});
  }

  canDeleteAuthor() {
    let formIsValid = true;
    let errors = {};

    this.props.courses.map(course => {
      if (course.authorId == this.state.author) {
        errors.title = 'You can delete only lazy authors';
        formIsValid = false;
      }
    })
    this.setState({errors: errors});
    return formIsValid;
  }

  deleteAuthor(event) {
    event.preventDefault();

    this.setState({saving: true});

    if (!this.canDeleteAuthor()) {
      this.setState({saving: false});
      toastr.error(this.state.errors.title);
      return;
    }

    this.props.actions.deleteAuthor(this.state.author)
    .then(() => this.redirect())
    .catch(error => {
      toastr.error(error);
      this.setState({saving: false});
    });
  }

  redirect() {
    this.setState({saving: false});
    toastr.success('Author Deleted');
  }

  render() {
    return (
      <form>
        <h1>Manage Course</h1>

        <SelectInput
          name="authorId"
          label="Author"
          value={this.state.author}
          defaultOption="Select Author"
          options={this.state.authors}
          onChange={this.updateAuthorState} error={null}/>

        <input
          type="submit"
          disabled={this.state.saving}
          value={this.state.saving ? 'Saving...' : 'Save'}
          className="btn btn-primary"
          onClick={this.deleteAuthor}/>
      </form>
    );
  }
}

ManageAuthorPage.propTypes = {
  authors: PropTypes.array.isRequired,
  courses: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired
};

ManageAuthorPage.contextTypes = {
  router: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    authors: authorsFormatterForDropdown(state.authors),
    courses: state.courses
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(authorActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageAuthorPage);
