import expect from 'expect';
import {authorsFormatterForDropdown} from './selectors';

describe('Author Selectors', () => {
  describe('authorsFormatterForDropdown', () => {
    it('should return author data formatter for dropdown', () => {
      const authors = [
        {id: 'corry-house', firstName: 'Corry', lastName: 'House'},
        {id: 'scott-allen', firstName: 'Scott', lastName: 'Allen'}
      ];

      const expected = [
        {value: 'corry-house', text: 'Corry House'},
        {value: 'scott-allen', text: 'Scott Allen'}
      ];

      expect(authorsFormatterForDropdown(authors)).toEqual(expected);
    });
  });
});
